# Avirato Test

    Para inicializar la aplicación:

## `npm install`

## `npm run start`


    Para poder ver el contenido es necesario logerase en el sistema.
    Una vez loggeado, refrescamos la pagina para poder ver la interfaz, donde podremos ver todos los usuarios registrados dandole al boton *All Users* 

    Si queremos crear un usuario *New user* y completamos el formulario, cerramos el modal y para poder ver el nuevo usuario creado volvemos a darle al boton *All-user*

    Tambien encontraremos un input donde podremos traer al front un usuario (previamente regsitrado) por medio de su *id*

    Por ultimo cada tarjeta de cada usuario tiene dos botones *delete* y *edit*, como sus propios nombres indican, uno borra al usuario de la base de datos y otro lo edita, siendo este ultimo muy importante que seleccionemos el id del usuario a cambiar

    Todas estas peticieones han sido realizadas con una autenticación de un Bearer (el token generado al haber iniciado sesion)

