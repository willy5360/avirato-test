import "./App.css";
import { useForm } from "react-hook-form";
import { AdminLogin } from "./usecases/login.usecase";
import { UserCard } from "./components/UserCard";
import { AllUsersUseCase } from "./usecases/all-users.usecase";
import {  useState } from "react";
import { NewUserButton } from "./components/NewUserButton";
import { GetUserByIdForm } from "./components/GetUserByIdForm";

function App() {
    const [users, setUser] = useState("");

    const { register, handleSubmit } = useForm();
    const onSubmit = async (data) => {
        const useCase = new AdminLogin();
        return await useCase.execute(data);
    };

    const getUsers = async () => {
        const useCase = new AllUsersUseCase();
        return setUser(await useCase.execute());
    };


    return !localStorage.getItem("token") ? (
        <form className="container mt-5 p-3 bg-color-transparent" onSubmit={handleSubmit(onSubmit)}>
            <h1 className=" white-color">Login</h1>
            <div className="mb-3">
                <label htmlFor="email" className="form-label white-color">
                    Email
                </label>
                <input
                    type="text"
                    className="form-control"
                    id="email"
                    aria-describedby="emailHelp"
                    {...register("email")}
                />
            </div>
            <div className="mb-3">
                <label htmlFor="password" className="form-label white-color">
                    Password
                </label>
                <input
                    type="password"
                    className="form-control"
                    id="password"
                    {...register("password")}
                />
            </div>
            <button type="submit" className="btn btn-primary">
                Sign in
            </button>
        </form>
    ) : (
        <div className="container p-5">
            <div className="row d-flex justify-content-center">
              <button className="btn btn-success col-4" onClick={() => getUsers()}>All users</button>
              <NewUserButton  />
            </div>
            <GetUserByIdForm />
            <UserCard users={users} />
        </div>
    );
}

export default App;
