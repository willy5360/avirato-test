import axios from "axios";

const baseUrl = "https://examen.avirato.com/client/";

function configurationGet(url) {
    return {
        method: "get",
        url: baseUrl.concat(url),
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
        },
    };
}

const config = {
    headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
};

export class ApiRepository {
    getAllUsers() {
        const dataUser = axios(configurationGet("get")).then((res) => res.data);
        return dataUser;
    }

    getUser(data) {
        const dataUser = axios(configurationGet(`get/one/${data.id}`)).then(
            (res) => res.data
        );
        return dataUser;
    }

    async searchUser() {
        return await axios.get(baseUrl.concat("get/search")).data;
    }

    async newUser(data) {
        return await axios
            .post(baseUrl.concat("post"), data, config)
            .then((response) => {
                console.log("aqui response", response);
                localStorage.setItem("users", ...response.data);
            })
            .catch((error) => console.log(error.response));
    }
    async modifyUser(data) {
        return await axios
            .put(baseUrl.concat("put"), data, config)
            .then((response) => console.log(response))
            .catch((error) => console.log(error.response));
    }

    async deleteUser(id) {
        return await axios.delete(baseUrl.concat(`delete/${id}`), config).data;
    }

    async login(data) {
        return await axios
            .post("https://examen.avirato.com/auth/login", data)
            .then((response) =>
                localStorage.setItem("token", response.data.access_token)
            )
            .catch((error) => console.log(error.message));
    }
}
