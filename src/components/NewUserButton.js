import React from "react";
import { useForm } from "react-hook-form";
import { NewUserUseCase } from "../usecases/new-user.usecase";

export const NewUserButton = () => {

    const { register, handleSubmit } = useForm();
    const onSubmit = async (data) => {
        const useCase = new NewUserUseCase()
        return await useCase.execute(data)
    }

    return (
        <div className="col">
            <button
                type="button"
                className="btn btn-primary"
                data-bs-toggle="modal"
                data-bs-target="#newUser"
            >
                New User
            </button>

            <div
                className="modal fade"
                id="newUser"
                tabIndex="-1"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">
                                New user
                            </h5>
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                            ></button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="mb-3">
                                    <label
                                        htmlFor="nombre"
                                        className="form-label"
                                    >
                                        nombre
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="nombre"
                                        aria-describedby="nombre"
                                        {...register("nombre")}
                                    />
                                </div>
                                <div className="mb-3">
                                    <label
                                        htmlFor="telefono"
                                        className="form-label"
                                    >
                                        telefono
                                    </label>
                                    <input
                                        type="tel"
                                        className="form-control"
                                        id="telefono"
                                        {...register("telefono")}
                                    />
                                </div>
                                <div className="mb-3">
                                    <label
                                        htmlFor="email"
                                        className="form-label"
                                    >
                                        email
                                    </label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        id="email"
                                        {...register("correo")}
                                    />
                                </div>
                                <div className="mb-3">
                                    <label
                                        htmlFor="fechaNacimiento"
                                        className="form-label"
                                    >
                                        fechaNacimiento
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="fechaNacimiento"
                                        {...register("fechaNacimiento")}
                                    />
                                </div>
                                <button
                                    type="submit"
                                    className="btn btn-primary"
                                >
                                    Save Changes
                                </button>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal"
                            >
                                Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
