import React, { useEffect, useState } from "react";
import { DeleteUserById } from "../usecases/delete-user.usecas";
import { ModifyForm } from "./ModifyForm";

export const UserCard = ({ users }) => {
    const [user, setUser] = useState("");

    const deleteUser = async (id) => {
        const useCase = new DeleteUserById();
        return await useCase.execute(id);
    };

    useEffect(() => {
        users &&
            setUser(
                users.map((user) => {
                    return (
                        <div key={user.id.toString()} className="card container m-3">
                            <div className="card-body">
                                <h5 className="card-title text-center"> {user.nombre.toUpperCase()}</h5>
                                <p className="card-text">Telefono: {user.telefono}</p>
                                <p className="card-text"> Correo: {user.correo}</p>
                                <p className="card-text">
                                    Fecha de nacimiento: 
                                    {user.fechaNacimiento}
                                </p>
                                <div className="row d-flex justify-content-around">
                                    <button
                                        onClick={() => deleteUser(user.id)}
                                        className="btn btn-danger col-3"
                                    >
                                        Delete
                                    </button>
                                    <button
                                        type="button"
                                        className="btn btn-primary col-3"
                                        data-bs-toggle="modal"
                                        data-bs-target="#edit-form"
                                    >
                                        Edit
                                    </button>

                                </div>
                            </div>
                        </div>
                    );
                })
            );
    }, [users]);

    return (
        <div className="container">
            {user}
            <div
                className="modal fade"
                id="edit-form"
                tabIndex="-1"
                aria-labelledby="edit-formLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body">
                        <ModifyForm />   
                        </div>
                        <div className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-secondary"
                                data-bs-dismiss="modal"
                            >
                                Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
