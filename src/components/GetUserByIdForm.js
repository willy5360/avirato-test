import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { UserByIdUseCase } from "../usecases/get-user-by-id.usecase";
import { UserCard } from "./UserCard";

export const GetUserByIdForm = () => {

    const [user, setUser] = useState([])

    const { register, handleSubmit } = useForm();
    const onSubmit = async (data) => {
        const useCase = new UserByIdUseCase();
        return setUser([await useCase.execute(data)]) 
    };


    return (
        <>
            <form className="container p-5" onSubmit={handleSubmit(onSubmit)}>
                <div className="mb-3">
                    <label htmlFor="idUser" className="form-label get-by-id">
                        Get by id
                    </label>
                    <input
                        type="number"
                        className="form-control"
                        id="idUser"
                        {...register("id")}
                    />
                </div>
                <button type="submit" className="btn btn-primary">
                    Get User
                </button>
            </form>
            <UserCard users={user} />
        </>
    );
};
