import React from 'react'
import { useForm } from "react-hook-form";
import { EditUser } from '../usecases/edit-user.usecase';


export const ModifyForm = () => {

    const { register, handleSubmit } = useForm();
    const onSubmit = async (data) => {
        const useCase = new EditUser()
        return await useCase.execute(data)
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
                <div className="mb-3">
                    <label htmlFor="nombre" className="form-label">
                        nombre
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="nombre"
                        aria-describedby="nombre"
                        {...register("nombre")}
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="telefono" className="form-label">
                        telefono
                    </label>
                    <input
                        type="tel"
                        className="form-control"
                        id="telefono"
                        {...register("telefono")}
    
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="email" className="form-label">
                        email
                    </label>
                    <input
                        type="email"
                        className="form-control"
                        id="email"
                        {...register("correo")}
    
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="fechaNacimiento" className="form-label">
                        fechaNacimiento
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="fechaNacimiento"
                        {...register("fechaNacimiento")}
    
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="id_user" className="form-label">
                        id
                    </label>
                    <input
                        type="number"
                        className="form-control"
                        id="id_user"
                        {...register("id")}
    
                    />
                </div>
                <button type="submit" className="btn btn-primary">
                    Save Changes
                </button>
            </form> 
      )
}
