import { ApiRepository } from "../repositories/api.repository";


export class AdminLogin{

    async execute(data){
        const repository = new ApiRepository()
        return await repository.login(data)
    }
}