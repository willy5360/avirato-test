import { ApiRepository } from "../repositories/api.repository";

export class AllUsersUseCase{

    async execute(){
        const repository = new ApiRepository()
        return await repository.getAllUsers()
    }
}