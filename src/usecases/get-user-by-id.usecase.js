import { ApiRepository } from "../repositories/api.repository";

export class UserByIdUseCase{

    async execute(id){
        const repository = new ApiRepository()
        return await repository.getUser(id)
    }
}