import { ApiRepository } from "../repositories/api.repository";

export class DeleteUserById {

    async execute(id){
        const repository = new ApiRepository()
        return await repository.deleteUser(id)
    }
}