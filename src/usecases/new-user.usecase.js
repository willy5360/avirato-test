import { ApiRepository } from "../repositories/api.repository";

 
export class NewUserUseCase {

    async execute(data){
        const repository = new ApiRepository()
        return await repository.newUser(data)
    }
}