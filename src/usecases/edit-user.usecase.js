import { ApiRepository } from "../repositories/api.repository";

export class EditUser{

    async execute(data){
        const repository = new ApiRepository()
        return await repository.modifyUser(data)
    }
}